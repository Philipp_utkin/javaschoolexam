package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (zeroCheck(inputNumbers)) {
            int[] array = listToArray(inputNumbers);
            quickSort(array, 0, array.length - 1);
            if (ValidHeight(array.length) != 0) {
                int height = ValidHeight(array.length);
                int[][] result = new int[height][height * 2 - 1];
                fill(result, array, height);
                return result;
            } else return new int[0][0];
        }
        else throw new CannotBuildPyramidException();
    }

    private static void quickSort(int[] array, int low, int high) {
        if (array.length == 0)
            return;
        if (low >= high)
            return;

        int middle = low + (high - low) / 2;
        int opora = array[middle];
        int i = low, j = high;

        while (i <= j) {
            while (array[i] < opora) {
                i++;
            }

            while (array[j] > opora) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j)
            quickSort(array, low, j);

        if (high > i)
            quickSort(array, i, high);
    }

    private int ValidHeight(int elementsNumber){
        int count = 0;
        while (elementsNumber > 0){
            count++;
            elementsNumber -= count;
        }
        if (elementsNumber == 0) return count;
        else {
            throw new CannotBuildPyramidException();
        }
    }

    private boolean zeroCheck(List<Integer> inputNumbers){
        boolean oneNotZero = false;
        for(Integer i : inputNumbers) {
            if (i == null) return false;
            else if (i != 0) oneNotZero = true;
        }
        return oneNotZero;
    }

    private void fill(int [][] result, int[]array, int height){
        int listCount = 0, step = 0;
        int midCount = height - 1;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < i + 1; j++) {
                result[i][midCount + step + j] = array[listCount];
                listCount++;
                step++;
            }
            step = 0;
            if (!(midCount - 1 < 0)) midCount -= 1;
        }
    }

    private int[] listToArray(List<Integer> inputNumbers){
        int[] array = new int[inputNumbers.size()];
        for (int i = 0; i < inputNumbers.size(); i++) {
            array[i] = inputNumbers.get(i);
        }
        return array;
    }


}
