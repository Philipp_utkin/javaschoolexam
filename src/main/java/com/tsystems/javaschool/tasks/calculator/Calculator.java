package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Stack;
import java.lang.StringBuilder;
import java.text.DecimalFormat;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || statement.isEmpty()) return null;
        try{
            if (isValid(statement)){
                String str = statement;
                str = format(statement);
                str = convert(str);
                Double result = count(str);
                str = round(result);
                return str;
            }
        }
        catch (ArithmeticException e){
            return null;
        }
        return null;
    }

    private static boolean isOperator(char ch) {
        return ch == '*' || ch == '/' || ch == '+' || ch == '-';
    }

    private boolean isOpenBracket(char ch){
        return ch == '(';
    }

    private boolean isCloseBracket(char ch){
        return ch == ')';
    }

    private boolean isDelimiter(char c){
        return c == ' ';
    }

    private int priority(char operator) {
        switch (operator) {
            case '^' : return 3;
            case '*' :
            case '/' : return 2;
            case '+' :
            case '-' : return 1;
        }
        return 0;
    }

    private String convert(String expression) {
        // Use StringBuilder to append string is faster than string concatenation
        StringBuilder sb = new StringBuilder();
        // Use a stack to track operations
        Stack<Character> stack = new Stack<Character>();
        // Convert expression string to char array
        char[] chars = expression.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char ch = chars[i];
            if (isDelimiter(ch)) continue;
            if (Character.isDigit(ch) || ch == '.') {
                // Number, simply append to the result
                while ((Character.isDigit(chars[i])|| chars[i] == '.') && i < chars.length) {
                    sb.append(chars[i]);
                    i++;
                    if (i == chars.length) break;
                }
                // Use space as delimiter
                sb.append(' ');
            }
            else if (ch == '(') {
                // Left parenthesis, push to the stack
                stack.push(ch);
            }
            else if (ch == ')') {
                // Right parenthesis, pop and append to the result until meet the left parenthesis
                while (stack.peek() != '(') {
                    sb.append(stack.pop()).append(' ');
                }
                // Don't forget to pop the left parenthesis out
                stack.pop();
            }
            else if (isOperator(ch)) {
                // Operator, pop out all higher priority operators first and then push it to the stack
                while (!stack.isEmpty() && priority(stack.peek()) >= priority(ch)) {
                    sb.append(stack.pop()).append(' ');
                }
                stack.push(ch);
            }
        }
        // Finally pop out whatever left in the stack and append to the result
        while(!stack.isEmpty()) {
            sb.append(stack.pop()).append(' ');
        }
        return sb.toString();
    }

    private double count(String expression) throws ArithmeticException {
        double result = 0;
        Stack<Double> stack = new Stack<Double>(); //Temporary stack
        char[] input = expression.toCharArray();
        for (int i = 0; i < input.length; i++) //for each symbol in string
        {
            //If symbol is a digit, read full number and add it to Stack
            if (Character.isDigit(input[i]))
            {
                String a = "";
                while (!isDelimiter(input[i]) && !isOperator(input[i])) //Пока не разделитель
                {
                    a += input[i]; //adding digits
                    i++;
                    if (i == input.length) break;
                }
                stack.push(Double.parseDouble(a)); //pushing number to stack
                i--;
            }
            else if (isOperator(input[i]))
            {
                //Take two last numbers from stack and do the required operation
                double a = stack.pop();
                double b = stack.pop();
                switch (input[i])
                {
                    case '+': result = b + a; break;
                    case '-': result = b - a; break;
                    case '*': result = b * a; break;
                    case '/':
                        {
                            if (a == 0) throw new ArithmeticException();
                            result = b / a;
                            break;
                        }
                }
                stack.push(result); //result goes to stack
            }
        }
        return stack.peek(); //take the last result from stack
    }

    private String format(String expression){
        ArrayList<String> list = new ArrayList<>();
        char[] charExpression = expression.toCharArray();
        for (char symbol : charExpression) {
            if (isOperator(symbol) || isCloseBracket(symbol) || isOpenBracket(symbol)) list.add(" " + Character.toString(symbol) + " ");
            else list.add(Character.toString(symbol));
        }
        String listString = "";
        for (String s : list)
        {
            listString += s;
        }
        return listString;
    }

    private boolean isValidSymbol (char input){
        return !(!isOperator(input) && !isDelimiter(input) && input != '.' && !Character.isDigit(input) && input!= '(' && input != ')');
    }

    private boolean isValidFirstSymbol (char input){
        return (Character.isDigit(input) || isOpenBracket(input) || isCloseBracket(input));
    }

    private boolean isValid(String expression){
        if (expression.isEmpty()) return false;
        char temp, input;
        char[] chars = expression.toCharArray();
        if (!isValidFirstSymbol(chars[0])) return false;
        else {
            temp = chars[0];
            int bracketCount = 0;
            if (temp == '(') bracketCount++;
            for (int i = 1; i < chars.length; i++) {
                input = chars[i];
                if (!isValidSymbol(input)) return false;
                if (temp == input && (isOperator(temp) || temp == '.')) return false;
                if (isOpenBracket(input) || isCloseBracket(input)) bracketCount++;
                temp = input;
            }
            return !(bracketCount!= 0 && bracketCount % 2 != 0);
        }
    }

    private String round (Double number){
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(number).replace(',','.');
    }
}
